$(document).ready(function() {
    $('#experimentForm').submit(function(e) {
        e.preventDefault();

        var formData = {
            name: $('#name').val(),
            type: $('#type').val()
            // Gather other fields similarly
        };

        $.ajax({
            url: '/your-endpoint', // Your API endpoint
            type: 'POST', // or 'PUT'
            contentType: 'application/json',
            data: JSON.stringify(formData),
            success: function(response) {
                console.log(response);
                alert('Experiment submitted successfully');
                // Clear the form or redirect the user
            },
            error: function(xhr, status, error) {
                console.error(error);
                alert('An error occurred');
            }
        });
    });
});
