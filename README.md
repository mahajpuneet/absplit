# Hermes Testing Platform

## Overview
Hermes is an A/B Testing Platform designed to create and analyze experiments. It includes a web interface for managing experiments and viewing detailed analysis results.

## Project Structure
The project comprises two primary HTML files:
1. `index.html` - For creating and viewing experiments.
2. `experiment-details.html` - For displaying detailed analysis results of experiments.

### index.html
This file serves as the main interface of the Hermes platform, offering functionalities such as:
- Creating new experiments with various configurations including experiment name, UUID preference, start and stop times, user percentage, variations, and metrics.
- Viewing a list of existing experiments presented in a tabular format, where each experiment name links to its detailed analysis page.

### experiment-details.html
This file presents a comprehensive analysis of an experiment, covering aspects like:
- Metric Name, Value, Delta, P-Value, and Bucket Variation for each evaluated metric.
- Delta is calculated as \((B-A)/A \times 100\)% where \( B \) is the metric value in the even row and \( A \) is the metric value in the previous odd row.
- P-Values are shown only in even rows for clarity.

## Usage
To engage with the platform:
1. Launch `index.html` in a web browser.
2. To create a new experiment, fill out the provided form under the "Create Experiment" tab. To view existing experiments, switch to the "View Experiments" tab.
3. Click on an experiment's name in the list to access its detailed analysis on `experiment-details.html`.

### Starting the Server
Ensure the Go server is active for data retrieval and submission, typically at `http://localhost:8080`.

### Adding New Experiments
For adding a new experiment:
1. Go to the "Create Experiment" tab.
2. Enter the required details and submit.

### Viewing Experiment Details
For examining existing experiments:
1. Switch to the "View Experiments" tab.
2. Browse through the list of experiments.
3. Select an experiment to view its detailed analysis.

## Dependencies
- Materialize CSS for styling.
- JavaScript Fetch API for interacting with the server.

Refer to the comments within the HTML files for detailed information on each file and their functionalities.
